1. select model , speed , hd
   from pc
   where price <500

2.select maker
   from product
   where product.type='printer'
   group by maker

3.Select model,ram,screen
   from laptop
   where price>1000

4.Select code,model,color,type,price
   from printer c
   where c.color='y'

5.Select model,speed,hd
   from pc p
   where (p.cd='12x' or p.cd='24x') and p.price<600

7.select distinct product.model, pc.price
   from Product
    join pc on product.model = pc.model
where maker = 'B'
   union
select distinct product.model, laptop.price
from product
join laptop on product.model=laptop.model
where maker='B'
union
select distinct product.model, printer.price
from product
    join printer on product.model=printer.model
where maker='B';

8.Select distinct maker
from product
where type='pc'
except
select distinct maker
from product
where type='laptop'

9.select dictinct product.maker
   from pc
       inner join product on pc.model = product.model
   where pc.speed >= 450

10.select model, price
from printer
where price=(select max(price)
    from printer)

11.Select avg(speed)
from pc

12.Select avg(speed)
from laptop
where price>1000

13.Select avg(pc.speed)
from pc , product
where pc.model=product.model and product.maker='A'

14.Select s.class , s.name, c.country
from ships s
    left join classes c on s.class = c.class
where c.numGuns >= 10

15.select hd
from pc
group by (hd)
having count (model) >= 2

16.Select distinct p1.model , p2.model , p1.speed , p1.ram
from pc p1, pc p2
where p1.speed=p2.speed and p1.ram=p2.ram and p1.model>p2.model

17.Select distinct p.type, p.model , l.speed
from laptop l
    join product p on l.model=p.model
where l.speed<(select min(speed)
    from pc)

18.Select  distinct product.maker , printer.price
from product , printer
where product.model=printer.model and printer.color='y'
  and printer.price=(select min(price)
    from printer
    where color='y')

19.Select product.maker, avg(screen)
from laptop
    left join product on product.model=laptop.model
group by product.maker

20.Select maker , count(model)
from product
where type='pc'
group by product.maker
having count(model)>=3

21.Select product.maker, max(pc.price)
from product , pc
where product.model=pc.model
group by product.maker

22.Select pc.speed, avg(pc.price)
from pc
where speed>600
group by pc.speed

23.Select distinct  maker
from product p1
    join pc p2 on p1.model = p2.model
where p2.speed>=750 and
    maker in(select maker
    from product p1
    join laptop l on p1.model=l.model
    where l.speed>=750)

27.Select product.maker, avg(pc.hd)
from product, pc
where product.model=pc.model
  and product.maker in(
    select distinct maker
    from product
    where product.type='printer')
group by maker

28.Select count(maker)
from product
where maker in(
    select maker
    from product
    group by maker
    having count(model)=1)

31.Select class, country
From Classes
Where bore >= 16

33.Select ship
from outcomes
where battle='North Atlantic' and result='sunk'

38.Select country
from Classes
where type = 'bb'
intersect
select country
from Classes
where type = 'bc'

42.Select ship, battle
from outcomes
where result = 'sunk'

44.Select r.name
from(select ships.name as name
    from ships
    union
    select outcomes.ship as name
    from outcomes) as r
where r.name like 'R%'

45.select name
from ships
where name like '% % %'
union
select ship
from outcomes
where ship like '% % %'

49.Select r.name from (
    select ships.name as name, classes.bore as bore
    from ships
    join classes on ships.class = classes.class
    union
    select outcomes.ship as name, classes.bore as bore
    from outcomes
    join classes on outcomes.ship = classes.class) as r
where r.bore = 16

50.Select distinct battle
from outcomes
    join ships on outcomes.ship=ships.name
where ships.class='Kongo'

55.select launched, class
from classes, ships
where ships.class=classes.class and
select c.class, t1.year
from classes c left join (
select class, min(launched) as year
from ships
group by class)
as t1 nn c.class=t1.class

62.Select sum(t1.sum1)
from (select sum(inc) sum1
from Income_o
where date < '2001-04-15'
union
select -sum(out) sum1
from Outcome_o
where date < '2001-04-15'
) t1

63.Select name
from passenger
where ID_psg in
(select ID_psg
from Pass_in_trip
group by ID_psg, place
having count(*)>1)

67.Select count(*) qty
from (Select Top 1 With TIES count(*) qty, town_from, town_to
from Trip
group by town_from, town_to
order by qty desc) as t1

71.Select p.maker
from product p
left join pc on pc.model=p.model
where p.type = 'PC'
group by p.maker
having count(pc.model) = count(p.model)

85.select maker
from product
group by maker
having count(distinct type) = 1 and
(min(type) = 'printer' or
(min(type) = 'pc' and count(model) >= 3))

 86.select maker, case count(distinct type)
when 2 then min(type) + '/' + max(type)
when 1 then max(type)
when 3 then 'laptop/pc/printer'
end
from Product
group by maker

89.With sr as (select count(model) qty
from Product
group by maker)
select maker, count(model) qty
from Product
group by maker
having count(model) = (select MAX(qty)
from sr)
or count(model) = (select MIN(qty)
from sr)

90.Select t1.maker, t1.model, t1.type
from (select
row_number() over (order by model) p1,
row_number() over (order by model desc) p2,*
from product) t1
Where p1 > 3 and p2 > 3


