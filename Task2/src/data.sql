insert into client(idclient, firstnameclient, lastnameclient, loginclient, ratingclient, ageclient, linkedcard, phonenumberclient)
values (1,'Лена','Гаврилова','lgvrlv',5,18,true,880053535);
insert into client(idclient, firstnameclient, lastnameclient, loginclient, ratingclient, ageclient, linkedcard, phonenumberclient)
values (2,'Ксения','Катаргина','kkkseniaaa',5,19,true,123458900);
insert into client(idclient, firstnameclient, lastnameclient, loginclient, ratingclient, ageclient, linkedcard, phonenumberclient)
values (3,'Айри','Маири','airimairi',4,18,true,880533535);

insert into driver(iddriver, firtnamedriver, lastnamedriver, logindriver, ratingdriver, agedriver, drivingexperience, phonenumberdriver)
values (1,'Илья','Ивлев','lays',3,19,2,880347535);
insert into driver(iddriver, firtnamedriver, lastnamedriver, logindriver, ratingdriver, agedriver, drivingexperience, phonenumberdriver)
values (2,'Далия','Сафина','horse',5,20,3,885355735);
insert into driver(iddriver, firtnamedriver, lastnamedriver, logindriver, ratingdriver, agedriver, drivingexperience, phonenumberdriver)
values (3,'Лена','Гусарова','lena',4,19,2,83225735);

insert into car(idcar, brand, model, yearofissue, number, color, comforttype)
values (1,'Tesla','Model X',2015,'В345УК77','red',true);
insert into car(idcar, brand, model, yearofissue, number, color, comforttype)
values (2,'Lada','2114',2011,'Г928ЦД23','black',false);
insert into car(idcar, brand, model, yearofissue, number, color, comforttype)
values (3,'Toyota','Corolla',2013,'Л763ДУ73','white',true);


insert into "order"(idorder, startorder, endorder, duration, waiting, price, client_id, driver_id, car_id)
values (1,'11:12:03','11:36:57','0:23:54',true,563,1,2,1);
insert into "order"(idorder, startorder, endorder, duration, waiting, price, client_id, driver_id, car_id)
values (2,'18:10:00','19:32:00','1:22:00',false,782,2,1,2);
insert into "order"(idorder, startorder, endorder, duration, waiting, price, client_id, driver_id, car_id)
values (3,'00:57:10','01:14:10','0:17:00',false,142,3,2,1);

update client
set phonenumberclient = '793777777'
where idClient = 2;
update "order"
set waiting = true,
    duration          = '00:21:00'
where idorder = 3;

