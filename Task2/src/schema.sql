drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists "order",client,driver;

create table client (
    idClient bigint unique not null,
    firstNameClient char(20),
    lastNameClient char(20),
    ratingClient integer check (ratingClient >=0 and ratingClient <=5),
    ageClient integer check (ageClient >= 14) ,
    linkedCard boolean,
    phoneNumberClient integer
);

create table driver (
    idDriver bigint unique not null,
    firtNameDriver char(20),
    lastNameDriver char(20),
    ratingDriver integer check (ratingDriver >=0 and ratingDriver<=5 ),
    ageDriver integer check ( ageDriver>18 and ageDriver<=65 ),
    drivingExperience integer check ( drivingExperience >1 ),
    phoneNumberDriver integer
);

create table car (
    idCar bigint unique not null ,
    brand char(30),
    model char(30),
    yearOfIssue integer check ( yearOfIssue >2010 ),
    number char(8),
    color char(20),
    comfortType boolean
);

create table "order" (
idOrder bigint unique not null,
startOrder time,
endOrder time,
duration time,
waiting boolean,
price integer
);

alter table driver
    add phoneNumberDriver integer;


alter table "order" add client_id bigint;
alter table "order" add foreign key(client_id) references client(idClient);

alter table "order" add driver_id bigint;
alter table "order" add foreign key(driver_id) references driver(idDriver);

alter table "order" add car_id bigint;
alter table "order" add foreign key(car_id) references car(idCar);

