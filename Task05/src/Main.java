import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentsRepository;
import repositories.StudentsRepositoryJdbcImpl;
import services.StudentsService;
import services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);
        studentsRepository.delete(6L);
        Student upStudent = new Student(1L, "Lena", "Gavrilova", 19);
        studentsRepository.update(upStudent);
        studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(18);
    }
}

