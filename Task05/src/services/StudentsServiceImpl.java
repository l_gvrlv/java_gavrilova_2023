package services;

import dto.StudentSignUp;
import models.Student;
import repositories.StudentsRepository;

public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = new Student(form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword());

        studentsRepository.save(student);
    }
}

