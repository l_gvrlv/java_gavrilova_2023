package app;

import dto.SignUpForm;
import mappers.Mappers;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryFilesImpl;
import services.UsersService;
import services.UsersServiceImpl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

public class Main {
    public static void main(String[] args) throws IOException {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("user.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Лена", "Гаврилова",
                "lena.gavrilova2408@mail.ru", "qwerty007"));
        usersService.signUp(new SignUpForm("Ксения", "Катаргина",
                "kseniaaa@gmail.com", "qwerty"));
        usersService.signUp(new SignUpForm("Илья", "Ивлев",
                "snek@gmail.com", "qwerty00"));
        usersService.signUp(new SignUpForm("Айрина", "Латыпова",
                "airimairi@gmail.com", "qwerty0"));
        usersService.signUp(new SignUpForm("Далия", "Сафина",
                "netlita@gmail.com", "qwerty7"));
        usersService.signUp(new SignUpForm("Яна", "Негина",
                "stopapup@gmail.com", "qwerty07"));

        System.out.println(usersRepository.findAll());
        System.out.println(usersRepository.findById("325df8de-621c-4288-b646-d27b254820a2"));
        usersRepository.deleteById("eeb2023b-619d-4273-ba5c-0d7483e1eaab");
        usersRepository.delete(usersRepository.findAll().get(15));
        usersRepository.update(usersRepository.findAll().get(2));
        System.out.println(usersRepository.findAll());
    }
}


