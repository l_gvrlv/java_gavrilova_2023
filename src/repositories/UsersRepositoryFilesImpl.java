package repositories;


import models.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private final String path = "C:\\Users\\lenag\\Desktop\\SummerPractice\\java_gavrilova_2023\\user.txt";

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() throws IOException {
        List<User> users = new ArrayList<>();
        String[] line = Files.readAllLines(Paths.get(path)).toArray(new String[0]);
        for (int i = 0; i < line.length; i++) {
            String[] parametrs = line[i].split("\\|");
            users.add(new User(parametrs[0], parametrs[1], parametrs[2], parametrs[3], parametrs[4]));
        }
        return users;
    }

    @Override
    public void save(User entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
                String userAsString = userToString.apply(entity);
                bufferedWriter.write(userAsString);
                bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) throws IOException {
        System.out.println("Еnter the new user data.");

        Scanner sc = new Scanner(System.in);
        String firstName = sc.nextLine();
        String lastName = sc.nextLine();
        String email = sc.nextLine();
        String password = sc.nextLine();

        User upUser = new User(entity.getId(), firstName, lastName, email, password);

        save(upUser);
        delete(entity);

        File file = new File(path);
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
        bufferedWriter.newLine();
        bufferedWriter.write(upUser.toString());
        bufferedWriter.close();
        }


    @Override
    public void delete(User entity) throws IOException {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(String idToBeRemove) throws IOException {
        Path input = Paths.get("user.txt");
        Path temp = Files.createTempFile("temp", ".txt");
        Stream<String> lines = Files.lines(input);
        try (BufferedWriter writer = Files.newBufferedWriter(temp)) {
            lines
                    .filter(line -> !line.startsWith(idToBeRemove))
                    .forEach(line -> {
                        try {
                            writer.write(line);
                            writer.newLine();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }

        Files.move(temp, input, StandardCopyOption.REPLACE_EXISTING);
        System.out.println("Done!");
    }

    @Override
    public User findById(String id) throws IOException {
        User defaultUser = null;
        for (User u : findAll()) {
            if (u.getId().equals(id)) {
                defaultUser = u;
            }
        }
        return defaultUser;
    }

}
