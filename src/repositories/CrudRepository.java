package repositories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface CrudRepository<ID, T> {
    List<T> findAll() throws IOException;

    void save(T entity);

    void update(T entity) throws IOException;

    void delete(T entity) throws IOException;

    void deleteById(String id) throws IOException;

    T findById(String id) throws IOException;

}

