package services;

import dto.StudentSignUp;

public interface StudentsService {
    void signUp(StudentSignUp form);
}

